package glLib

import (
	"github.com/go-gl/gl/v3.2-core/gl"
	"image"
	"image/draw"
	_ "image/jpeg"
	_ "image/png"
	"os"
	"unsafe"
)

type Texture struct {
	id uint32
	Width, Height int32
	InternalFormat int32
	ImageFormat uint32
	WrapS int32
	WrapT int32
}

func NewTextureFromFile(name string, wrapS, wrapT int32) (*Texture, error) {
	imgData, err := os.Open(name)
	if err != nil {
		return nil, err
	}
	defer imgData.Close()
	img, _, err := image.Decode(imgData)
	if err != nil {
		return nil, err
	}
	return NewTexture(img, wrapS, wrapT), nil
}

func NewTextureFromMemory(pixels unsafe.Pointer, width, height int32, format, wrapS, wrapT, min, mag int32) *Texture {
	t := new(Texture)
	gl.GenTextures(1, &t.id)
	t.WrapS = wrapS
	t.WrapT = wrapT
	t.ImageFormat = uint32(format)
	t.InternalFormat = format
	t.Width = width
	t.Height = height
	t.Bind(gl.TEXTURE0)
	defer t.UnBind()
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, t.WrapS)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, t.WrapT)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, min)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, mag)
	gl.TexImage2D(gl.TEXTURE_2D, 0, t.InternalFormat, t.Width, t.Height, 0, t.ImageFormat, gl.UNSIGNED_BYTE, pixels)
	return t
}

func NewTexture(img image.Image, wrapS, wrapT int32) *Texture {
	t := new(Texture)
	rgba := image.NewRGBA(img.Bounds())
	draw.Draw(rgba, rgba.Bounds(), img, image.Pt(0, 0), draw.Src)
	gl.GenTextures(1, &t.id)
	t.WrapS = wrapS
	t.WrapT = wrapT
	t.ImageFormat = gl.RGBA
	t.InternalFormat = gl.SRGB_ALPHA
	t.Width = int32(rgba.Rect.Size().X)
	t.Height = int32(rgba.Rect.Size().Y)
	t.Bind(gl.TEXTURE0)
	defer t.UnBind()
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, t.WrapS)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, t.WrapT)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
	gl.TexImage2D(gl.TEXTURE_2D, 0, t.InternalFormat, t.Width, t.Height, 0, t.ImageFormat, gl.UNSIGNED_BYTE, gl.Ptr(rgba.Pix))
	return t
}

func (t *Texture) Bind(tex uint32) {
	gl.ActiveTexture(tex)
	gl.BindTexture(gl.TEXTURE_2D, t.id)
}

func (t *Texture) UnBind() {
	gl.BindTexture(gl.TEXTURE_2D, 0)
}

func (t *Texture) GetID() uint32 {
	return t.id
}

func (t *Texture) Delete() {
	gl.DeleteTextures(1, &t.id)
}