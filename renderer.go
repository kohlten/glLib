package glLib

import (
	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/go-gl/mathgl/mgl32"
	"math"
)

type Renderer struct {
	shader *Shader
	quad uint32
}

func NewRenderer(shader *Shader) *Renderer {
	r := new(Renderer)
	r.shader = shader
	r.initRenderData()
	return r
}

func (r *Renderer) DrawSprite(texture *Texture, pos mgl32.Vec2, size mgl32.Vec2, angle float32, color mgl32.Vec4) {
	gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
	r.shader.Use()
	model := mgl32.Mat4{}
	trans := mgl32.Translate3D(pos.X(), pos.Y(), 0)
	model = model.Add(trans)
	trans = mgl32.Translate3D(0.5 * size.X(), 0.5 * size.Y(), 0.0)
	model = model.Mul4(trans)
	rotate := mgl32.Rotate2D(angle * (math.Pi / 180.0))
	model = model.Mul4(rotate.Mat4())
	trans = mgl32.Translate3D(-(0.5 * size.X()), -(0.5 * size.Y()), 0.0)
	model = model.Mul4(trans)
	scale := mgl32.Scale3D(size.X(), size.Y(), 1.0)
	model = model.Mul4(scale)
	r.shader.SetMatrix4("model", model, false)
	r.shader.SetVector4f("spriteColor", color, false)
	texture.Bind(gl.TEXTURE0)
	defer texture.UnBind()
	gl.BindVertexArray(r.quad)
	gl.DrawArrays(gl.TRIANGLES, 0, 6)
	gl.BindVertexArray(0)
}

func (r *Renderer) initRenderData() {
	vertices := []float32{
		// Pos       Tex
	 	0.0, 1.0, 0.0, 1.0,
		1.0, 0.0, 1.0, 0.0,
		0.0, 0.0, 0.0, 0.0,

		0.0, 1.0, 0.0, 1.0,
		1.0, 1.0, 1.0, 1.0,
		1.0, 0.0, 1.0, 0.0,
	}
	gl.GenVertexArrays(1, &r.quad)
	vbo := uint32(0)
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, len(vertices) * 4, gl.Ptr(vertices), gl.STATIC_DRAW)
	gl.BindVertexArray(r.quad)
	gl.EnableVertexAttribArray(0)
	gl.VertexAttribPointer(0, 4, gl.FLOAT, false, 4 * 4, nil)
	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
	gl.BindVertexArray(0)
}