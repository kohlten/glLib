package glLib

import (
	"fmt"
	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/go-gl/mathgl/mgl32"
	"io/ioutil"
	"os"
	"strings"
)

type Shader struct {
	id uint32
}

func NewShader(vertexName, fragmentName, geometryName string) (*Shader, error) {
	s := new(Shader)
	vertexSource, err := readAll(vertexName)
	if err != nil {
		return nil, err
	}
	fragmentSource, err := readAll(fragmentName)
	if err != nil {
		return nil, err
	}
	geometrySource := ""
	if geometryName != "" {
		geometrySource, err = readAll(fragmentName)
		if err != nil {
			return nil, err
		}
	}
	err = s.compile(vertexSource, fragmentSource, geometrySource)
	if err != nil {
		return nil, err
	}
	return s, nil
}

func (s *Shader) Use() {
	gl.UseProgram(s.id)
}

func (s *Shader) SetFloat(name string, val float32, useShader bool) {
	if useShader {
		s.Use()
	}
	gl.Uniform1f(gl.GetUniformLocation(s.id, gl.Str(name + "\x00")), val)
}

func (s *Shader) SetInt(name string, val int32, useShader bool) {
	if useShader {
		s.Use()
	}
	gl.Uniform1i(gl.GetUniformLocation(s.id, gl.Str(name + "\x00")), val)
}

func (s *Shader) Set2f(name string, x, y float32, useShader bool) {
	if useShader {
		s.Use()
	}
	gl.Uniform2f(gl.GetUniformLocation(s.id, gl.Str(name + "\x00")), x, y)
}

func (s *Shader) SetVector2f(name string, vec mgl32.Vec2, useShader bool) {
	s.Set2f(name, vec.X(), vec.Y(), useShader)
}


func (s *Shader) Set3f(name string, x, y, z float32, useShader bool) {
	if useShader {
		s.Use()
	}
	gl.Uniform3f(gl.GetUniformLocation(s.id, gl.Str(name + "\x00")), x, y, z)
}

func (s *Shader) SetVector3f(name string, vec mgl32.Vec3, useShader bool) {
	s.Set3f(name, vec.X(), vec.Y(), vec.Z(), useShader)
}

func (s *Shader) Set4f(name string, x, y, z, w float32, useShader bool) {
	if useShader {
		s.Use()
	}
	gl.Uniform4f(gl.GetUniformLocation(s.id, gl.Str(name + "\x00")), x, y, z, w)
}

func (s *Shader) SetVector4f(name string, vec mgl32.Vec4, useShader bool) {
	s.Set4f(name, vec.X(), vec.Y(), vec.Z(), vec.W(), useShader)
}

func (s *Shader) SetMatrix4(name string, mat mgl32.Mat4, useShader bool) {
	if useShader {
		s.Use()
	}
	gl.UniformMatrix4fv(gl.GetUniformLocation(s.id, gl.Str(name + "\x00")), 1, false, &mat[0])
}

func (s *Shader) Delete() {
	gl.DeleteProgram(s.id)
}

func readAll(name string) (string, error) {
	fd, err := os.Open(name)
	if err != nil {
		return "", err
	}
	data, err := ioutil.ReadAll(fd)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func (s *Shader) compile(vertexSource, fragmentSource, geometrySource string) error {
	vertex := gl.CreateShader(gl.VERTEX_SHADER)
	cSource, free := gl.Strs(vertexSource  + "\x00")
	gl.ShaderSource(vertex, 1, cSource, nil)
	free()
	gl.CompileShader(vertex)
	err := s.check(vertex, "VERTEX")
	if err != nil {
		return err
	}
	fragment := gl.CreateShader(gl.FRAGMENT_SHADER)
	cSource, free = gl.Strs(fragmentSource  + "\x00")
	gl.ShaderSource(fragment, 1, cSource, nil)
	free()
	gl.CompileShader(fragment)
	err = s.check(fragment, "FRAGMENT")
	if err != nil {
		return err
	}
	geometry := uint32(0)
	if geometrySource != "" {
		geometry = gl.CreateShader(gl.GEOMETRY_SHADER)
		cSource, free := gl.Strs(geometrySource  + "\x00")
		gl.ShaderSource(geometry, 1, cSource, nil)
		free()
		gl.CompileShader(geometry)
		err = s.check(geometry, "GEOMETRY")
		if err != nil {
			return err
		}
	}
	s.id = gl.CreateProgram()
	gl.AttachShader(s.id, vertex)
	gl.AttachShader(s.id, fragment)
	if geometrySource != "" {
		gl.AttachShader(s.id, geometry)
	}
	gl.LinkProgram(s.id)
	err = s.check(s.id, "PROGRAM")
	if err != nil {
		return err
	}
	gl.DeleteShader(vertex)
	gl.DeleteShader(fragment)
	if geometrySource != "" {
		gl.DeleteShader(geometry)
	}
	return nil
}


func (s *Shader) check(object uint32, name string) error {
	success := int32(0)
	infoLog := strings.Repeat("\x00", 1025)
	if name != "PROGRAM" {
		gl.GetShaderiv(object, gl.COMPILE_STATUS, &success)
		if success != 1 {
			gl.GetShaderInfoLog(object, 1024, nil, gl.Str(infoLog))
			return fmt.Errorf("failed to compile %v", infoLog)
		}
	} else {
		gl.GetProgramiv(object, gl.LINK_STATUS, &success)
		if success != 1 {
			gl.GetProgramInfoLog(object, 1024, nil, gl.Str(infoLog))
			return fmt.Errorf("failed to link %v", infoLog)
		}
	}
	return nil
}