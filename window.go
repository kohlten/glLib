package glLib

import (
	"errors"
	"fmt"
	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/veandco/go-sdl2/sdl"
)

const (
	WindowDefaultMonitor              = 0
	WindowDefaultMultiSamplingSamples = 0
	WindowDefaultOpenGlVersionMajor   = 3
	WindowDefaultOpenGlVersionMinor   = 2
	WindowDefaultVsync                = false
	WindowDefaultSmothing             = false
	WindowDefaultMultisampling        = false
	WindowDefaultFullscreen           = false
	WindowDefaultWindowedFullscreen   = false
	WindowDefaultUseGl                = false
	WindowDefaultUseDoubleBuffer      = true
	WindowDefaultShowWindow           = true
	WindowDefaultResizeable           = false
)

type WindowSettings struct {
	Width                uint
	Height               uint
	Monitor              uint
	MultisamplingSamples uint
	OpenGlVersionMajor   uint
	OpenGlVersionMinor   uint
	Name                 string
	Vsync                bool
	Smoothing            bool
	Multisampling        bool
	Fullscreen           bool
	WindowedFullscreen   bool
	UseGl                bool
	UseDoubleBufferGl    bool
	ShowWindow           bool
	Resizeable           bool
}

type Window struct {
	settings  WindowSettings
	Window    *sdl.Window
	Renderer  *sdl.Renderer
	GlContext sdl.GLContext
}

func NewWindowSettings() WindowSettings {
	settings := WindowSettings{}
	settings.Monitor = WindowDefaultMonitor
	settings.MultisamplingSamples = WindowDefaultMultiSamplingSamples
	settings.OpenGlVersionMajor = WindowDefaultOpenGlVersionMajor
	settings.OpenGlVersionMinor = WindowDefaultOpenGlVersionMinor
	settings.Vsync = WindowDefaultVsync
	settings.Smoothing = WindowDefaultSmothing
	settings.Multisampling = WindowDefaultMultisampling
	settings.Fullscreen = WindowDefaultFullscreen
	settings.WindowedFullscreen = WindowDefaultWindowedFullscreen
	settings.UseGl = WindowDefaultUseGl
	settings.UseDoubleBufferGl = WindowDefaultUseDoubleBuffer
	settings.ShowWindow = WindowDefaultShowWindow
	settings.Resizeable = WindowDefaultResizeable
	return settings
}

func NewWindow(settings WindowSettings) (*Window, error) {
	if settings.Width == 0 || settings.Height == 0 {
		return nil, errors.New("invalid window settings")
	}
	err := sdl.Init(sdl.INIT_EVERYTHING)
	if err != nil {
		return nil, err
	}
	if settings.Smoothing {
		sdl.SetHint(sdl.HINT_RENDER_SCALE_QUALITY, "2")
	}
	numDisplays, err := sdl.GetNumVideoDisplays()
	if err != nil {
		return nil, err
	}
	if settings.Monitor >= uint(numDisplays) {
		fmt.Println("WINDOW: Warning! Monitor is greater than the amount of displays!")
		settings.Monitor = 0
	}
	displayMode, _ := sdl.GetCurrentDisplayMode(int(settings.Monitor))
	if settings.Width > uint(displayMode.W) || settings.Height > uint(displayMode.H) {
		return nil, errors.New("width or height is greater than that Monitor's size")
	} else if settings.Width == 0 || settings.Height == 0 {
		return nil, errors.New("width or height is zero")
	}
	x := uint(getWindowPosition(int(settings.Monitor)))
	window, err := createWindow(x, displayMode, settings)
	if err != nil {
		return nil, err
	}
	return window, nil
}

func (window *Window) GetSize() (uint, uint) {
	return window.settings.Width, window.settings.Height
}

func (window *Window) SetSize(w, h uint) {
	window.settings.Width = w
	window.settings.Height = h
}

func (window *Window) GetSettings() WindowSettings {
	return window.settings
}

func (window *Window) Free() {
	if window.GlContext != nil {
		sdl.GLDeleteContext(window.GlContext)
	}
	if window.Renderer != nil {
		window.Renderer.Destroy()
	}
	window.Window.Destroy()
	sdl.Quit()
}

func getWindowPosition(monitor int) int {
	x := 0
	for i := 0; i < monitor; i++ {
		bounds, _ := sdl.GetDisplayBounds(i)
		x += int(bounds.W)
	}
	return x
}

func getSdlFlags(settings WindowSettings) (int, int) {
	windowFlags := 0
	if settings.Fullscreen {
		windowFlags |= sdl.WINDOW_FULLSCREEN
	}
	if settings.WindowedFullscreen {
		windowFlags |= sdl.WINDOW_FULLSCREEN_DESKTOP
	}
	if settings.UseGl {
		windowFlags |= sdl.WINDOW_OPENGL
	}
	if settings.Resizeable {
		windowFlags |= sdl.WINDOW_RESIZABLE
	}
	if !settings.ShowWindow {
		windowFlags |= sdl.WINDOW_HIDDEN
	}
	if settings.Resizeable {
		windowFlags |= sdl.WINDOW_RESIZABLE
	}
	rendererFlags := sdl.RENDERER_ACCELERATED
	if settings.Vsync && !settings.UseGl {
		rendererFlags |= sdl.RENDERER_PRESENTVSYNC
	}
	return windowFlags, rendererFlags
}

func (window *Window) initGl() error {
	err := errors.New("")
	if window.settings.UseGl || window.settings.Multisampling {
		window.GlContext, err = window.Window.GLCreateContext()
		if err != nil {
			return err
		}
		err = window.Window.GLMakeCurrent(window.GlContext)
		if err != nil {
			return err
		}
		err = gl.Init()
		if err != nil {
			return err
		}
		gl.Viewport(0, 0, int32(window.settings.Width), int32(window.settings.Height))
	}

	if window.settings.UseGl {
		if window.settings.Vsync {
			err = sdl.GLSetSwapInterval(1)
		} else {
			err = sdl.GLSetSwapInterval(0)
		}
		if err != nil {
			return err
		}
		_ = sdl.GLSetAttribute(sdl.GL_CONTEXT_FLAGS, sdl.GL_CONTEXT_FORWARD_COMPATIBLE_FLAG)
		_ = sdl.GLSetAttribute(sdl.GL_DOUBLEBUFFER, 1)
		_ = sdl.GLSetAttribute(sdl.GL_DEPTH_SIZE, 24)
		_ = sdl.GLSetAttribute(sdl.GL_STENCIL_SIZE, 8)
		if window.settings.OpenGlVersionMajor == 0 && window.settings.OpenGlVersionMinor == 0 {
			window.settings.OpenGlVersionMajor = 3
			window.settings.OpenGlVersionMinor = 2
		}
		if window.settings.OpenGlVersionMajor >= 3 && window.settings.OpenGlVersionMinor >= 2 {
			err = sdl.GLSetAttribute(sdl.GL_CONTEXT_PROFILE_MASK, sdl.GL_CONTEXT_PROFILE_CORE)
			if err != nil {
				return err
			}
		}
		err = sdl.GLSetAttribute(sdl.GL_CONTEXT_MAJOR_VERSION, int(window.settings.OpenGlVersionMajor))
		if err != nil {
			return err
		}
		err = sdl.GLSetAttribute(sdl.GL_CONTEXT_MINOR_VERSION, int(window.settings.OpenGlVersionMinor))
		if err != nil {
			return err
		}
		if window.settings.UseDoubleBufferGl {
			err = sdl.GLSetAttribute(sdl.GL_DOUBLEBUFFER, 1)
			if err != nil {
				return err
			}
		}

	}
	if window.settings.Multisampling {
		if window.settings.MultisamplingSamples%2 != 0 {
			fmt.Println("WINDOW: Warning! Samples must be divisible by 2! Disabling multisampling.")
			window.settings.Multisampling = false
		} else {
			gl.Enable(gl.MULTISAMPLE)
			err := sdl.GLSetAttribute(sdl.GL_MULTISAMPLEBUFFERS, int(window.settings.MultisamplingSamples))
			if err != nil {
				return err
			}
			err = sdl.GLSetAttribute(sdl.GL_MULTISAMPLESAMPLES, int(window.settings.MultisamplingSamples))
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func createWindow(positionX uint, displayMode sdl.DisplayMode, settings WindowSettings) (*Window, error) {
	window := new(Window)
	window.settings = settings
	windowFlags, rendererFlags := getSdlFlags(settings)
	positionX = positionX + ((uint(displayMode.W / 2)) - settings.Width/2)
	positionY := uint(displayMode.H)/2 - settings.Height/2
	sdlWindow, err := sdl.CreateWindow(settings.Name, int32(positionX), int32(positionY), int32(settings.Width), int32(settings.Height), uint32(windowFlags))
	if err != nil {
		return nil, err
	}
	window.Window = sdlWindow
	err = window.initGl()
	if err != nil {
		return nil, err
	}
	if !settings.UseGl {
		window.Renderer, err = sdl.CreateRenderer(window.Window, -1, uint32(rendererFlags))
		if err != nil {
			return nil, err
		}
	}
	return window, nil
}
