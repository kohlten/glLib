package glLib

import (
	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/go-gl/mathgl/mgl32"
	"math/rand"
	"time"
)

type Particle struct {
	Position, Velocity mgl32.Vec2
	Color              mgl32.Vec4
	Life               float32
}

type ParticleGenerator struct {
	shader    *Shader
	texture   *Texture
	vao       uint32
	particles []Particle
	lastUsedParticle int
}

func NewParticleGenerator(shader *Shader, texture *Texture, amount uint) *ParticleGenerator {
	gen := new(ParticleGenerator)
	rand.Seed(time.Now().UnixNano())
	gen.shader = shader
	gen.texture = texture
	vbo := uint32(0)
	particleQuad := []float32{
		0.0, 1.0, 0.0, 1.0,
		1.0, 0.0, 1.0, 0.0,
		0.0, 0.0, 0.0, 0.0,

		0.0, 1.0, 0.0, 1.0,
		1.0, 1.0, 1.0, 1.0,
		1.0, 0.0, 1.0, 0.0,
	}
	gl.GenVertexArrays(1, &gen.vao)
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, len(particleQuad) * 4, gl.Ptr(particleQuad), gl.STATIC_DRAW)
	gl.BindVertexArray(gen.vao)
	gl.EnableVertexAttribArray(0)
	gl.VertexAttribPointer(0, 4, gl.FLOAT, false, 4 * 4, nil)
	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
	gl.BindVertexArray(0)
	gen.particles = make([]Particle, amount)
	return gen
}

func (pg *ParticleGenerator) Update(dt float32, newParticles uint, pos, vel, offset mgl32.Vec2) {
	for i := uint(0); i < newParticles; i++ {
		loc := pg.firstUnused()
		pg.respawnParticle(&pg.particles[loc], pos, vel, offset)
	}
	for i := 0; i < len(pg.particles); i++ {
		p := &pg.particles[i]
		p.Life -= dt
		if p.Life > 0 {
			randOffset := mgl32.Vec2{float32(rand.Int() % 10) - 5, float32(rand.Int() % 10) - 5}
			p.Position = p.Position.Sub(p.Velocity.Mul(dt)).Add(randOffset)
			p.Color[3] -= dt
		}
		pg.particles[i] = *p
	}
}

func (pg *ParticleGenerator) Draw() {
	gl.BlendFunc(gl.SRC_ALPHA, gl.ONE)
	pg.shader.Use()
	pg.texture.Bind(gl.TEXTURE0)
	for _, particle := range pg.particles {
		if particle.Life > 0 {
			pg.shader.SetVector2f("offset", particle.Position, false)
			pg.shader.SetVector4f("color", particle.Color, false)
			gl.BindVertexArray(pg.vao)
			gl.DrawArrays(gl.TRIANGLES, 0, 6)
			gl.BindVertexArray(0)
		}
	}
	gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
}

func (pg *ParticleGenerator) firstUnused() int {
	for i := pg.lastUsedParticle; i < len(pg.particles); i++ {
		if pg.particles[i].Life <= 0 {
			pg.lastUsedParticle = i
			return i
		}
	}
	for i := 0; i < pg.lastUsedParticle; i++ {
		if pg.particles[i].Life <= 0 {
			pg.lastUsedParticle = i
			return i
		}
	}
	pg.lastUsedParticle = 0
	return pg.lastUsedParticle
}

func (pg *ParticleGenerator) respawnParticle(particle *Particle, pos, vel, offset mgl32.Vec2) {
	randOffset := float32((rand.Int() % 100) - 50)  / 10.0
	rColor := float32(rand.Int() % 100) / 100
	particle.Position = pos.Add(mgl32.Vec2{randOffset, randOffset}).Add(offset)
	particle.Color = mgl32.Vec4{rColor, rColor, rColor, 1.0}
	particle.Life = 1.0
	particle.Velocity = vel.Mul(0.1)
}
