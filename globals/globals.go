package globals

var values map[string]interface{}

func init() {
	values = make(map[string]interface{})
}

func Add(name string, val interface{}) {
	values[name] = val
}

func Remove(name string) {
	delete(values, name)
}

func Get(name string) interface{} {
	val, ok := values[name]
	if !ok {
		return nil
	}
	return val
}
