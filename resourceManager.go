package glLib

type ResourceManager struct {
	shaders map[string]*Shader
	textures map[string]*Texture
}

func NewResourceManager() *ResourceManager {
	rm := new(ResourceManager)
	rm.shaders = make(map[string]*Shader)
	rm.textures = make(map[string]*Texture)
	return rm
}

func (rm *ResourceManager) SaveShader(name string, shader *Shader) {
	rm.shaders[name] = shader
}

func (rm *ResourceManager) GetShader(name string) *Shader {
	shader, ok := rm.shaders[name]
	if !ok {
		return nil
	}
	return shader
}

func (rm *ResourceManager) SaveTexture(name string, texture *Texture) {
	rm.textures[name] = texture
}

func (rm *ResourceManager) GetTexture(name string) *Texture {
	texture, ok := rm.textures[name]
	if !ok {
		return nil
	}
	return texture
}

func (rm *ResourceManager) Clear() {
	for _, v := range rm.shaders {
		v.Delete()
	}
	for _, v := range rm.textures {
		v.Delete()
	}
	rm.shaders = make(map[string]*Shader)
	rm.textures = make(map[string]*Texture)
}
